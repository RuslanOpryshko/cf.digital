"use strict"
//first project
let projects = document.querySelector(".projects");
let first_project = document.querySelector(".first");
let item_hover_1 = document.querySelector(".item_hover");
let hover_description_1 = document.querySelector(".hover__desription_1");
let hover_name__first = document.querySelector(".hover_name__first");
let hover_text__first = document.querySelector(".hover_text__first");


//second project
let second_project = document.querySelector(".second");
let item_hover_2 = document.querySelector(".item_hover_2");
let hover_description_2 = document.querySelector(".hover__desription_2");
let hover_name__second = document.querySelector(".hover_name__second");
let hover_text__second = document.querySelector(".hover_text__second");

//third project
let third_project = document.querySelector(".third");
let item_hover_3 = document.querySelector(".item_hover_3");
let hover_description_3 = document.querySelector(".hover__desription_3");
let hover_name__third = document.querySelector(".hover_name__third");
let hover_text__third = document.querySelector(".hover_text__third");

//fours project
let fours_project = document.querySelector(".fours");
let item_hover_4 = document.querySelector(".item_hover_4");
let hover_description_4 = document.querySelector(".hover__desription_4");
let hover_name__fours = document.querySelector(".hover_name__fours");
let hover_text__fours = document.querySelector(".hover_text__fours");

//first project function
first_project.addEventListener("mouseover", function() {
    item_hover_1.classList.add('show_hover');
})
first_project.addEventListener("mouseout", function() {
    item_hover_1.classList.remove('show_hover');
})

hover_name__first.addEventListener("mouseover", function() {
    hover_description_1.classList.add('show_desription');
})
hover_text__first.addEventListener("mouseover", function() {
    hover_description_1.classList.add('show_desription');
})

//second project function
second_project.addEventListener("mouseover", function() {
    item_hover_2.classList.add('show_hover');
})
second_project.addEventListener("mouseout", function() {
    item_hover_2.classList.remove('show_hover');
})

hover_name__second.addEventListener("mouseover", function a() {
    hover_description_2.classList.add('show_desription');
    second_project.classList.add('move_hover');
})
hover_text__second.addEventListener("mouseover", function() {
    hover_description_2.classList.add('show_desription');
    second_project.classList.add('move_hover');
})

//third project function
third_project.addEventListener("mouseover", function() {
    item_hover_3.classList.add('show_hover');
})
third_project.addEventListener("mouseout", function() {
    item_hover_3.classList.remove('show_hover');
})

hover_name__third.addEventListener("mouseover", function() {
    hover_description_3.classList.add('show_desription');
    third_project.classList.add("move_hover");
})

hover_text__third.addEventListener("mouseover", function() {
    hover_description_3.classList.add('show_desription');
    third_project.classList.add('move_hover');
})

//fours project function
fours_project.addEventListener("mouseover", function() {
    item_hover_4.classList.add('show_hover');
})
fours_project.addEventListener("mouseout", function() {
    item_hover_4.classList.remove('show_hover');
})

hover_name__fours.addEventListener("mouseover", function c() {
    hover_description_4.classList.add('show_desription');
    fours_project.classList.add('move_hover');
})

hover_text__fours.addEventListener("mouseover", function() {
    hover_description_4.classList.add('show_desription');
    fours_project.classList.add('move_hover');
})